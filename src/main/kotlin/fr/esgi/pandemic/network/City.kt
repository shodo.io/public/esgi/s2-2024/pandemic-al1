package fr.esgi.pandemic.network

private const val OUTBREAK_LEVEL = 3

data class City(val name: String, val infectionLevel: Int = 0) {
    fun infect(): City {
        return if (infectionLevel < OUTBREAK_LEVEL) {
            copy(infectionLevel = infectionLevel + 1)
        }
        else {
            this
        }
    }
}
