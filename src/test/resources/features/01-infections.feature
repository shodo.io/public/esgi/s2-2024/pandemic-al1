Feature: City Infection

  Rule: Infection marker should be incremented

    Scenario: First infection
      Given LONDON has not been infected
      When LONDON is infected
      Then LONDON infection level should be 1

    Scenario: Second infection
      Given LONDON has been infected 1 time
      When LONDON is infected
      Then LONDON infection level should be 2

    Scenario: Third infection
      Given LONDON has been infected 2 times
      When LONDON is infected
      Then LONDON infection level should be 3


  Rule: Fourth Infection  should not increment markers
    Scenario: Fourth infection
      Given LONDON has been infected 3 times
      When LONDON is infected
      Then LONDON infection level should be 3
