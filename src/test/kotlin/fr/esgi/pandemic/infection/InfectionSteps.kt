package fr.esgi.pandemic.infection

import fr.esgi.pandemic.network.City
import io.cucumber.java.ParameterType
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.assertj.core.api.Assertions


class InfectionSteps {
    private val network: MutableMap<String, City> = mutableMapOf()

    @ParameterType("PARIS|LONDON")
    fun cityName(param: String): String {
        return param
    }

    @Given("{cityName} has not been infected")
    fun notinfected(cityName: String) {
        network[cityName] = City(cityName)
    }

    @Given("{cityName} has been infected {int} time(s)")
    fun infected(cityName: String, times: Int) {
        network.putIfAbsent(cityName, City(cityName))
        repeat(times){
            network[cityName] = network[cityName]!!.infect()
        }
    }

    @When("{cityName} is infected")
    fun paris_is_infected(cityName: String) {
        network[cityName] =
            network[cityName]?.infect() ?: throw IllegalStateException("city $cityName does not exist on network")
    }

    @Then("{cityName} infection level should be {int}")
    fun paris_infection_level_should_be(cityName: String, infectionLevel: Int) {
        Assertions.assertThat(network[cityName]!!.infectionLevel).isEqualTo(infectionLevel)
    }
}